﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{

    [Table("VideoElements")]
    public class VideoElement
    {

        // Common properties.
        [Key]
        public long Id { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public bool IsFolderElement { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string FileFormat { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string FileName { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string Path { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string FullPath { get; set; }

        // Video specific properties.
        [Required(ErrorMessage = "Field is required")]
        public TimeSpan Duration { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public double Framerate { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public int ResWidth { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public int ResHeight { get; set; }

        public string ThumbnailFile { get; set; }

    }

}
