﻿using Contracts;
using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{

    public class RepositoryWrapper : IRepositoryWrapper
    {

        private RepositoryContext mRepoContext;
        private IVideoElementRepository mVideoElements;


        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            mRepoContext = repositoryContext;

            //mRepoContext.Database.EnsureDeleted();
            mRepoContext.Database.EnsureCreated();
        }


        public IVideoElementRepository VideoElements
        {
            get
            {
                if (mVideoElements == null)
                {
                    mVideoElements = new VideoElementRepository(mRepoContext);
                }

                return mVideoElements;
            }
        }


    }

}
