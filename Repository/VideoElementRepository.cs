﻿using Contracts;
using Entities;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{

    public class VideoElementRepository : RepositoryBase<VideoElement>, IVideoElementRepository
    {

        public VideoElementRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {

        }

    }

}
