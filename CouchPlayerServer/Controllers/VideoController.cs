﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using static Common.MediaCrawler;

namespace CouchPlayerServer.Controllers
{
    public class VideoController : Controller
    {
        private IRepositoryWrapper mRepoWrapper;


        public VideoController(IRepositoryWrapper repoWrapper)
        {
            mRepoWrapper = repoWrapper;
        }


        [HttpGet]
        public IActionResult Index()
        {

            VideoElement tmpElem = new VideoElement();
            tmpElem.Id = 2;
            tmpElem.Title = "Test";

            tmpElem.IsFolderElement = false;
            tmpElem.FileFormat = "mp4";
            tmpElem.FileName = "test.mp4";
            tmpElem.Path = "C:\temp";
            tmpElem.FullPath = "C:\temp\test.mp4";
            tmpElem.Duration = new TimeSpan();
            tmpElem.Framerate = 60;
            tmpElem.ResWidth = 1920;
            tmpElem.ResHeight = 1080;


            //mRepoWrapper.VideoElements.Create(tmpElem);
            //mRepoWrapper.VideoElements.Save();

            var video = mRepoWrapper.VideoElements.FindAll();

            List<VideoElement> tmpVideos = mRepoWrapper.VideoElements.FindAll().ToList();

            return View(tmpVideos);
        }

        [HttpGet]
        public IActionResult GetAllVideos()
        {

            List<VideoElement> tmpVideos = mRepoWrapper.VideoElements.FindAll().ToList();


            return View();
        }


        [HttpGet]
        public IActionResult ShowVideo(int id)
        {

            List<VideoElement> tmpVideos = mRepoWrapper.VideoElements.FindAll().ToList();

            List<FileInfo> tmpLocalVideos = MediaCrawler.GetFilesByType(MediaTypes.Video, @"D:\HandtkeA\Vertraulich\tmp\Android_Backup\MobileBackup\Data\2018-09-04-132707\Video\Movies\Sonstige", true);

            foreach (var video in tmpLocalVideos)
            {

                bool tmpIsDuplicate = tmpVideos.Where(v => v.Title == video.Name).Any();

                if (!tmpIsDuplicate)
                {
                    VideoElement tmpElem = new VideoElement();

                    tmpElem.Title = video.Name;
                    tmpElem.IsFolderElement = false;
                    tmpElem.FileFormat = video.Extension.ToLower();
                    tmpElem.FileName = video.Name;
                    tmpElem.Path = video.FullName;
                    tmpElem.FullPath = "C:\temp\test.mp4";
                    tmpElem.Duration = new TimeSpan();
                    tmpElem.Framerate = 60;
                    tmpElem.ResWidth = 1920;
                    tmpElem.ResHeight = 1080;

                    mRepoWrapper.VideoElements.Create(tmpElem);
                    mRepoWrapper.VideoElements.Save();
                }

            }

            return View("Index", tmpVideos);
        }


    }
}